//
//  VehicleLocationTests.swift
//  vehicleMapTests
//
//  Created by Jakub Gosciniak on 8/28/19.
//  Copyright © 2019 Jakub Gosciniak. All rights reserved.
//

import XCTest
@testable import vehicleMap

class VehicleLocationTests: XCTestCase {
    
    func testDistanceSuccess() {
        let stub = VechicleLocationsStub()
        let location = stub.locations.first
        XCTAssertEqual(location?.distance(52.40, 16.92), 0)
        XCTAssertEqual(location?.distance(52.40, 16.93), 0.68064209749848)
        XCTAssertEqual(location?.distance(52.41, 16.92), 1.1127494035961194)
    }
    
    func testDistanceFailure() {
        let stub = VechicleLocationsStub()
        let location = stub.locations.first
        XCTAssertNotEqual(location?.distance(52.40, 16.92), 10)
    }

}
