//
//  VechicleLocationsStub.swift
//  vehicleMapTests
//
//  Created by Jakub Gosciniak on 8/28/19.
//  Copyright © 2019 Jakub Gosciniak. All rights reserved.
//

import UIKit
@testable import vehicleMap

class VechicleLocationsStub: NSObject {
    var locations: [VehicleLocation] = [VehicleLocation(52.40, 16.92), VehicleLocation(52.40, 17.0), VehicleLocation(52.40, 17.12), VehicleLocation(52.40, 17.23)]
}
