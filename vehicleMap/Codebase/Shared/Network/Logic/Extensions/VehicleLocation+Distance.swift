//
//  VehicleLocation+Distance.swift
//  vehicleMap
//
//  Created by Jakub Gosciniak on 8/28/19.
//  Copyright © 2019 Jakub Gosciniak. All rights reserved.
//

import UIKit
import CoreLocation

extension VehicleLocation {

    func distance(_ fromLat: Double, _ fromLong: Double) -> Double? {
        guard let longitude = self.longitude, let latitude = self.latitude else { return nil}
        return CLLocation(latitude: latitude, longitude: longitude).distance(from: CLLocation(latitude: fromLat, longitude: fromLong)) / 1000
    }
}
