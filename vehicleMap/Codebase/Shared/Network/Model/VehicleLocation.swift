//
//  VehicleLocation.swift
//  vehicleMap
//
//  Created by Jakub Gosciniak on 8/28/19.
//  Copyright © 2019 Jakub Gosciniak. All rights reserved.
//

import UIKit

class VehicleLocation: NSObject {
    var longitude: Double?
    var latitude: Double?
    
    init(_ lat: Double, _ long: Double) {
        super.init()
        self.latitude = lat
        self.longitude = long
    }
}
