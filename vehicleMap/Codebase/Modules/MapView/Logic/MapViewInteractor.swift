//
//  MapViewInteractor.swift
//  vehicleMap
//
//  Created by Jakub Gosciniak on 8/28/19.
//  Copyright © 2019 Jakub Gosciniak. All rights reserved.
//

import UIKit
import CoreLocation

protocol MapViewInteractorInput: class {
    var output: MapViewPresenterInput? { get set }
    func getVehiclesLocationsInRange(km: Double, _ latitude: Double, _ longitude: Double)
}

class MapViewInteractor: NSObject, MapViewInteractorInput {
    var output: MapViewPresenterInput?
    let locationManager = CLLocationManager()
    
    func getVehiclesLocationsInRange(km: Double, _ latitude: Double, _ longitude: Double) {
        let locations = VechicleLocationsStub().locations.filter({
            guard let distance = $0.distance(latitude, longitude) else { return false}
            return distance <= km })
        output?.presentVehiclesLocations(locations)
    }
}
