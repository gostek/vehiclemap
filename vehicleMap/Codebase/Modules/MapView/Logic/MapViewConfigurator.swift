//
//  MapViewConfigurator.swift
//  vehicleMap
//
//  Created by Jakub Gosciniak on 8/28/19.
//  Copyright © 2019 Jakub Gosciniak. All rights reserved.
//

import UIKit

class MapViewConfigurator: NSObject {
    
    static let sharedInstance = MapViewConfigurator()
    
    func configure(_ viewController: MapViewControllerInput) {
        let presenter = MapViewPresenter()
        presenter.output = viewController
        let interactor = MapViewInteractor()
        interactor.output = presenter
        viewController.output = interactor
    }
}
