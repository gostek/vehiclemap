//
//  MapViewPresenter.swift
//  vehicleMap
//
//  Created by Jakub Gosciniak on 8/28/19.
//  Copyright © 2019 Jakub Gosciniak. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewPresenterInput: class {
    var output: MapViewControllerInput? { get set }
    func presentVehiclesLocations(_ locations: [VehicleLocation])
}

class MapViewPresenter: NSObject, MapViewPresenterInput {
    weak var output: MapViewControllerInput?
    
    func presentVehiclesLocations(_ locations: [VehicleLocation]) {
        var annotationsArray: [MKPointAnnotation] = []
        for location in locations {
            guard let lat = location.latitude, let long = location.longitude else { continue }
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotationsArray.append(annotation)
        }
        
        self.output?.displayVehiclesAnnotations(annotationsArray)
    }
}
