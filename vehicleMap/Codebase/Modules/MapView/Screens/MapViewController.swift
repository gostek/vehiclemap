//
//  MapViewController.swift
//  
//
//  Created by Jakub Gosciniak on 8/26/19.
//

import UIKit
import MapKit

protocol MapViewControllerInput: class {
    var output: MapViewInteractorInput? { get set }
    func displayVehiclesAnnotations(_ annotations: [MKPointAnnotation])
}

class MapViewController: UIViewController, MapViewControllerInput {
    
    var output: MapViewInteractorInput?
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MapViewConfigurator.sharedInstance.configure(self)
        output?.getVehiclesLocationsInRange(km: 10, 52.40, 17.10)
    }
    
    func displayVehiclesAnnotations(_ annotations: [MKPointAnnotation]) {
        mapView.addAnnotations(annotations)
    }
}
